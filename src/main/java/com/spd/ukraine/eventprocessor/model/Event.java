package com.spd.ukraine.eventprocessor.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.spd.ukraine.eventprocessor.model.annotation.EventCustomSerializer;

import java.time.ZonedDateTime;
import java.util.*;

@EventCustomSerializer
public class Event {

    @JsonProperty("event_code")
    private  String eventCode;

    @JsonProperty("event_source")
    private String eventSource;

    @JsonProperty("event_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime eventDate;

    private Set<String > attributes;


    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Event Source:");
        sb.append(eventSource);
        sb.append(";");
        sb.append("Event Code:");
        sb.append(eventCode);
        sb.append(";");
        sb.append("Attributes:");
        sb.append(attributes);
        sb.append("\t");

        return sb.toString();
    }

    public Event(String eventSource, String eventCode, ZonedDateTime eventDate) {
        this.eventSource = eventSource;
        this.eventCode = eventCode;
        this.eventDate = eventDate;
    }

    public String getEventSource() {
        return eventSource;
    }

    public void setEventSource(String eventSource) {
        this.eventSource = eventSource;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public ZonedDateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(ZonedDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public Set<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<String> attributes) {
        this.attributes = attributes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return eventCode.equals(event.eventCode);
    }

    @Override
    public int hashCode() {
        return 31 + Objects.hash(eventCode);
    }
}
