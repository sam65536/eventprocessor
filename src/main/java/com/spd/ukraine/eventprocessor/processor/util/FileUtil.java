package com.spd.ukraine.eventprocessor.processor.util;

import com.spd.ukraine.eventprocessor.model.Event;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

    public static final String DIRECTORY_WITH_RAW_DATA = "JSON/rawdata/";
    public static final String DIRECTORY_FOR_PROCESSED_DATA = "JSON/processed/";

    public static void writeJsonToFile(List<Event> events) {

        for (Event event : events) {
            String dirName = DIRECTORY_FOR_PROCESSED_DATA + event.getEventDate().toLocalDate().toString() + "/";
            String fileName = dirName  + event.getClass().getSimpleName().toLowerCase() + ".json";
            Path dirPath = Paths.get(dirName);
            Path filePath = Paths.get(fileName);

            try {
                if (!Files.exists(dirPath)) {
                    Files.createDirectory(dirPath);
                }

                if (!Files.exists(filePath)) {
                    Files.createFile(filePath);
                }
            } catch (IOException e) {
                System.err.printf("error while creating file:", e.getCause());
            }


            try (FileOutputStream outputStream  = new FileOutputStream(fileName, true)) {
                byte[] eventBytes = (event.toString() + "\n").getBytes();
                outputStream.write(eventBytes);
            } catch (IOException e) {
                System.err.printf("error while writing to file:", e.getCause());
            }
        }
    }

    public static List<String> extractRawDataFromDirectory() {
        return extractRawDataFromDirectory(DIRECTORY_WITH_RAW_DATA);
    }

    private static List<String> extractRawDataFromDirectory(String dirPath) {
        List<String> result = new ArrayList<>();
        try {
            Files.list(Paths.get(dirPath)).forEach(file -> {
                result.addAll(extractRawDataFromFile(file.toString()));
            });
        } catch (IOException e) {
            System.err.printf("Error while extracting json: ", e.getCause());
        }
        return result;
    }

    private static List<String> extractRawDataFromFile(String filePath) {
        List<String> result = new ArrayList<>();
        try {
            result = Files.readAllLines(Paths.get(filePath));
        } catch (IOException e) {
            System.err.printf("Error while extracting json: ", e.getCause());
        }

        return result;
    }
}
