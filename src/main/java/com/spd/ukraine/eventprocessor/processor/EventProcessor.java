package com.spd.ukraine.eventprocessor.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spd.ukraine.eventprocessor.model.Event;
import com.spd.ukraine.eventprocessor.processor.util.FileUtil;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class EventProcessor {

    public List<Event> getRangedEvents(List<Event> events) {
        List<Event> result = new ArrayList<>();
        Map<Event, Set<String>> eventsMap = new HashMap<>();

        for (Event event : events) {
            eventsMap.merge(event, event.getAttributes(), (oldVal, newVal) -> oldVal.addAll(newVal) ? oldVal : oldVal);
        }

        for (Map.Entry<Event,Set<String>> entry : eventsMap.entrySet()) {
            Event event = entry.getKey();
            event.setAttributes(entry.getValue());
            result.add(event);
        }
        return result;
    }

    public List<Event> processRawData() {
       List<Event> result = new ArrayList<>();
       List<String> rawContent = FileUtil.extractRawDataFromDirectory();

        rawContent.forEach(row -> {
            Map<String, String> jsonMap = getJsonMap(row);
            result.add(extractEventFromJsonMap(jsonMap));
        });

        return result;
    }

    private Event extractEventFromJsonMap(Map<String, String> jsonMap) {
        String eventSource = jsonMap.get("event_source");
        String eventCode = jsonMap.get("event_code");

        ZonedDateTime eventDate = ZonedDateTime.parse(jsonMap.get("event_date"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        Event event = new Event(eventSource, eventCode, eventDate);

        Set<String> attributes = new HashSet<>();

        jsonMap.keySet().stream()
            .filter(e -> !e.contains("event_source"))
            .filter(e -> !e.contains("event_code"))
            .forEach(attributes::add);

        event.setAttributes(attributes);

        return event;
    }

    private Map<String, String> getJsonMap(String jsonData) {
        Map<String, String> result = new HashMap<>();

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            result = objectMapper.readValue(jsonData, HashMap.class);
        } catch (IOException e) {
            System.err.printf("Error while parsing json: ", e.getCause());
        }
        return result;
    }

    public static void main(String[] args) {
        EventProcessor eventProcessor = new EventProcessor();
        List<Event> events = eventProcessor.processRawData();
        List<Event> rangedEvents = eventProcessor.getRangedEvents(events);
        FileUtil.writeJsonToFile(rangedEvents);
    }
}